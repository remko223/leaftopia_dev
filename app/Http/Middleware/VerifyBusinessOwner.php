<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use DB;

class VerifyBusinessOwner {
    public function handle($request, Closure $next) {
        if (Auth::check()) { //Is logged in & admin
            $slug = $request->route('slug');
            $results = DB::table('crawled_data')
                ->where('slug', $slug)
                ->where('owner', Auth::user()->id)
                ->where('approved', '1')
                ->get();

            if (count($results) > 0) {
                return $next($request);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }
}
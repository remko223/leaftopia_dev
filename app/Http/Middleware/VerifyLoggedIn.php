<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class VerifyLoggedIn {
    public function handle($request, Closure $next) {
        if (Auth::check()) { //Is logged in
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}
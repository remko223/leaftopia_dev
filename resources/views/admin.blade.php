@extends ("layouts.layout")

@section('title', 'Leaftopia')
@section('javascript')
    <script src="/assets/js/listings.js"></script>
    <script src="/assets/js/accordion.js"></script>
@stop

@section('content')
    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <div class="dark-transp">
        <div class="listing-header">
            <div class="container">
                <h4>Admin Panel</h4>
            </div>
        </div>
        <div id="live-map"></div>
    </div>
    <section class="dark-text">
        <div class="container">
            <div id="error-container" class="reporting error clearfix" style="display: none;">
                <span class="fa fa-close"></span><h6>Some error or success goes here</h6>
            </div>
            @if (count($pending) > 0)
                <h3>Listings Pending Approval</h3>
                @foreach ($pending as $listing)
                    <div class="accordion" slug="{{ $listing->slug }}">
                        <div class="row">
                            <div class="ten columns">
                                <button>{{ $listing->name }}</button>
                            </div>
                            <div class="one column">
                                <a class="approve"><span class="fa fa-check"></span></a>
                            </div>
                            <div class="one column">
                                <a class="reject"><span class="fa fa-close"></span></a>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="row">
                                <div class="four columns"><h6>Name</h6></div>
                                <div class="eight columns"><h6>{{ $listing->name }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>Description</h6></div>
                                <div class="eight columns"><h6>{{ $listing->body }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>Address</h6></div>
                                <div class="eight columns"><h6>{{ $listing->address }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>City</h6></div>
                                <div class="eight columns"><h6>{{ $listing->city }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>State</h6></div>
                                <div class="eight columns"><h6>{{ $listing->state }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>Zip Code</h6></div>
                                <div class="eight columns"><h6>{{ $listing->zip_code }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>License Type</h6></div>
                                <div class="eight columns"><h6>{{ $listing->license_type }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>Phone</h6></div>
                                <div class="eight columns"><h6>{{ $listing->phone_number }}</h6></div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="four columns"><h6>Email Address</h6></div>
                                <div class="eight columns"><h6>{{ $listing->email_address }}</h6></div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <br />
            @endif
            <h3>Set Listing Owner</h3>
            <div class="row">
                <div class="six columns">
                    <?php $url = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] . "/listing/neta-brookline" : "Enter a listing URL"; ?>
                    <label>Listing URL</label>
                    <input type="text" name="listing-url" placeholder="{{ $url }}" />
                </div>
                <div class="six columns">
                    <label>Profile Email</label>
                    <input class="full-width" type="email" name="profile-email" placeholder="Enter an email address" />
                </div>
                <br />
                <button id="submit-claim-listing">Submit</button>
            </div>
        </div>
    </section>

    <script src="/assets/js/business.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc&callback=initMap" async defer></script>
    <script>
        var map;
        function initMap() {
            getCurrentLocation(function (loc) {
                var latlng = loc.split(",");
                var lat = parseFloat(latlng[0]);
                var lng = parseFloat(latlng[1]);

                //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                map = new google.maps.Map(document.getElementById('live-map'), {
                    center: {lat: lat, lng: lng},
                    zoom: 14
                });
            });
        }

        $(".approve").click(function(e){
            e.preventDefault();
            var slug = $(this).closest(".accordion").attr("slug");

            $.ajax({
                url: '/approve-listing',
                type: 'POST',
                data: { 'slug' : slug },
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        location.reload();
                    } else {
                        logMessage(data, "error");
                    }
                }, error: function (data) {
                    console.log(data);
                }
            });
        });

        $(".reject").click(function(e) {
            e.preventDefault();
            var slug = $(this).closest(".accordion").attr("slug");

            $.ajax({
                url: '/reject-listing',
                type: 'POST',
                data: { 'slug' : slug },
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        location.reload();
                    } else {
                        logMessage(data, "error");
                    }
                }, error: function (data) {
                    console.log(data);
                }
            });
        });

        $("#submit-claim-listing").click(function(e) {
            e.preventDefault();
            var fullURL = $(this).parent().find("input[type='text']").val();
            var slug = fullURL.substr(fullURL.lastIndexOf("/") + 1);
            var email = $(this).parent().find("input[type='email']").val();

            $.ajax({
                url: '/claim-listing',
                type: 'POST',
                data: { 'slug' : slug, 'email' : email },
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    if (data == "1") {
                        logMessage("Successfully assigned listing to " + email, "success")
                    } else {
                        logMessage(data, "error");
                    }
                }, error: function (data) {
                    console.log(data); //Internal server error don't display
                }
            });
        });
    </script>
@stop
@extends ("layouts.layout")

@section('title', 'Leaftopia Map')
@section('javascript') <script src="/assets/js/listings.js"></script> @stop

@section('content')
    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <div class="row map-container">
        <div class="map-listings"></div> <!-- Listings will render here -->
        <div class="u-full-width">
            <script>
                //Create map
                function createMap() {
                    var map = new google.maps.Map(document.getElementById("listings-map"), {
                        scrollwheel: true,
                        zoom: 8
                    });

                    var address = "{{ $address or 0 }}";
                    if (address == 0) {
                        //Zoom to current location if no passed address
                        getCurrentLocation(function(location) {
                            var split = location.split(",");
                            var latLon = new google.maps.LatLng(parseFloat(split[0]), parseFloat(split[1]));
                            map.panTo(latLon);
                        });
                    } else {
                        geocodeAddress(address, function(latlng) {
                            if (latlng != undefined) {
                                map.panTo(latlng);
                            }
                        })
                    }

                    var liveMarkers = [];
                    var infoWindow = new google.maps.InfoWindow({ content: '' });
                    getListingsOnMapChange(map, 1, function(data) {
                        updateListings(data);
                    });

                    function updateListings(data) {
                        liveMarkers = removeOldMarkers(map, liveMarkers);

                        for (var i in data) {
                            var listing = data[i];
                            var pinIcon = new google.maps.MarkerImage(
                                    "/images/map-icon.svg",
                                    null,
                                    null,
                                    null,
                                    new google.maps.Size(50, 50)
                            );
                            var marker = new google.maps.Marker({
                                position: { lat: parseFloat(listing.latitude), lng: parseFloat(listing.longitude) },
                                map: map,
                                icon: pinIcon
                            });

                            marker["listing"] = listing;
                            marker.addListener('click', function() {
                                infoWindow.setContent('<p>' + this["listing"].title + '</p>');
                                infoWindow.open(map, this);
                            });

                            liveMarkers.push(marker);
                        }
                    }

                    google.maps.event.addListenerOnce(map, 'idle', function(){ //Update map for the first time
                        getListings(map, function(data) {
                            updateListings(data);
                        });
                    });

                    function removeOldMarkers(map, liveMarkers) {
                        var mapBounds = map.getBounds();

                        //Remove markers from map
                        var removed = [];
                        for (var i in liveMarkers) {
                            if (!mapBounds.contains(liveMarkers[i].getPosition())) {
                                liveMarkers[i].setMap(null);
                                removed.push(liveMarkers);
                            }
                        }

                        //Remove from livemarkers
                        for (var i in removed) {
                            liveMarkers.splice(liveMarkers.indexOf(removed[i]));
                        }

                        return liveMarkers;
                    }
                }
            </script>
            <div id="listings-map" class="full-height"></div>
        </div>
    </div>

    <script>
        //Calculate height on window resize
        $(window).resize(function() { calculateHeight(); });
        $(document).ready(function() { calculateHeight(); });

        function calculateHeight() {
            var headerHeight = $(".header-dark").outerHeight(true);
            $(".map-container").height($(window).outerHeight(true) - headerHeight);
        }
    </script>
    <!-- Google maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuwnA2oiCZgnZ5YBRhFdNTAYjvNM0I4BQ&callback=createMap" async defer></script>
@stop
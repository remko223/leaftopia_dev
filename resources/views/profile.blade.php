@extends ("layouts.layout")

@section('title', 'Leaftopia')
@section('javascript') <script src="/assets/js/listings.js"></script> @stop

@section('content')
    <?php $listingAppend = count($following) == 1 ? "y" : "ies"; ?>
    <div class="header-dark">
        <div class="container">
            @include('layouts.header')
        </div>
    </div>
    <div class="dark-transp">
        <div class="listing-header">
            <div class="container">
                <h4>{{ $name }}</h4>
                <p>{{ count($following) }} favorite dispensar{{ $listingAppend }}</p>
            </div>
        </div>
        <div id="live-map"></div>
    </div>
    <section class="dark-text">
        <div class="container">
            @if (count($listings) > 0)
                <h3 class="dark-color">Favorite Dispensar{{ $listingAppend }}</h3>
                <?php $count = 0; $colCount = 4; ?>

                @for ($i = 0; $i < count($listings); $i++)
                    @if ($count === 0) <div class="row row-margin"> @endif

                        <div class='three columns'>
                            <div class='listing'>
                                <a href='/listing/{{ $listings[$i]["slug"] }}' class='listing-slug'>
                                    <div class='listing-image' style="background-image: none"></div>
                                    <div class='listing-details'>
                                        <div>{{ $listings[$i]["name"] }}</div>
                                        <p><span class='fa fa-globe'></span> {{ $listings[$i]["city"] }}, {{ $listings[$i]["state"] }}</p>
                                        <p><span class='fa fa-location-arrow listing-distance' lat="{{ $listings[$i]["latitude"] }}" lng="{{ $listings[$i]["longitude"] }}"></span> </p>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <?php $count ++; ?> {{-- Increment count & close row --}}
                        @if ($count >= $colCount || $i + 1 == count($listings)) </div> <?php $count = 0; ?> @endif
                @endfor
            @endif

            @if (Auth::check() && Auth::user()->id == $id) {{-- User logged in --}}
            <?php
            $ownsListings = false;
            $hasPending = false;
            foreach ($owned as $listing) {
                if ($listing["approved"] == 1) $ownsListings = true;
                else $hasPending = true;
            }
            ?>

            @if ($ownsListings)
                <br />
                <h3>Dispensaries you own</h3>
                <table class="full-width listings">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($owned as $listing)
                        @if ($listing["approved"] == 1)
                            <tr>
                                <a href="/listing/{{ $listing["slug"] }}"><td><h6 class="dark-color">{{ $listing["name"] }}</h6></td></a>
                                <td><h6 class="dark-color">{{ $listing["city"] }}, {{ $listing["state"] }}</h6></td>
                                <td><a href="/edit-business/{{ $listing["slug"] }}"><span class="fa fa-pencil"></span></a></td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @endif

            @if ($hasPending)
                <br />
                <h3>Dispensaries pending approval</h3>
                <table class="full-width listings">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($owned as $listing)
                        @if ($listing["approved"] == 0)
                            <tr>
                                <a href="/listing/{{ $listing["slug"] }}"><td><h6 class="dark-color">{{ $listing["name"] }}</h6></td></a>
                                <td><h6 class="dark-color">{{ $listing["city"] }}, {{ $listing["state"] }}</h6></td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @endif
            @endif

            @if (Auth::check() && Auth::user()->id == $id) {{-- User logged in --}}
                <br />
                <button id="logout">Logout</button>
            @endif
        </div>
    </section>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCV0UScujyBhP1js2g5oLjV_ss9mOJiRc&callback=initMap" async defer></script>
    <script>
        var map;
        function initMap() {
            getCurrentLocation(function (loc) {
                var latlng = loc.split(",");
                var lat = parseFloat(latlng[0]);
                var lng = parseFloat(latlng[1]);

                //noinspection JSUnresolvedVariable,JSUnresolvedFunction
                map = new google.maps.Map(document.getElementById('live-map'), {
                    center: {lat: lat, lng: lng},
                    zoom: 14
                });

                $(".listing-distance").each(function() { //Find distance for listings
                    if (failedToLookupLocation) {
                        $(this).parent().remove(); //Remove if lookup failed
                    } else {
                        var dist = getDistanceFromLatLonInMiles(lat, lng, $(this).attr("lat"), $(this).attr("lng"));

                        //Can't directly add text, cache and prepend miles
                        var rounded = " " + (Math.round(dist * 10) / 10) + " Miles";
                        var milesCache = $(this).parent().children();
                        $(this).parent().text(rounded).prepend(milesCache);
                    }
                });

                $(".listing").each(function() {
                    var $distElement = $(this).find(".listing-distance");
                    var lat = $distElement.attr("lat");
                    var lng = $distElement.attr("lng");

                    getMapImageAtLocationCallback(lat + "," + lng, $(this).find(".listing-image"), function(url, $ele) {
                        $ele.css("background-image", "url(" + url + ")");
                    });
                });
            });
        }

        $("#logout").click(function(e) { //Logout functionality
            $.ajax({
                url: '/logout',
                type: 'POST',
                headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                success: function (data) {
                    location.reload();
                }
            });
        });
    </script>
@stop
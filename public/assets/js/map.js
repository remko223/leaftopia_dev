/**
 * Created by walkerchristie on 9/29/16.
 */

/**
 * Creates the map assigned to the passed element
 * and watches for changes, updating the map markers on
 * each change.
 * @param elementName Selector for map element
 */
function createMap(elementName) {
    //Initialize map
    var map = new google.maps.Map(document.querySelector(elementName), {
        scrollwheel: true,
        zoom: 8
    });

    //Zoom to current location
    getCurrentLocation(function(location) {
        var split = location.split(",");
        var latLon = new google.maps.LatLng(parseFloat(split[0]), parseFloat(split[1]));
        map.panTo(latLon);
    });

    //Callback for changing listings
    var liveMarkers = [];
    var infoWindow = new google.maps.InfoWindow({ content: '' });
    getListingsOnMapChange(map, 3, function(data) {
        liveMarkers = removeOldMarkers(map, liveMarkers);

        for (var i in data) {
            var listing = data[i];
            var marker = new google.maps.Marker({
                position: { lat: parseFloat(listing.latitude), lng: parseFloat(listing.longitude) },
                map: map,
                icon: "/images/map-icon.png"
            });

            marker["listing"] = listing;
            marker.addListener('click', function() {
                infoWindow.setContent('<a href="/listing/' + this["listing"].slug + '"><h6>' + this["listing"].title + '</h6></a>');
                infoWindow.open(map, this);
            });

            liveMarkers.push(marker);
        }
    });

    /**
     * Removes markers that cannot be seen anymore from
     * the map.
     * @param map Map to check markers
     * @param liveMarkers Array of markers
     * @returns {Array} Updated array of markers
     */
    function removeOldMarkers(map, liveMarkers) {
        var mapBounds = map.getBounds();

        //Remove markers from map
        var removed = [];
        for (var i in liveMarkers) {
            if (!mapBounds.contains(liveMarkers[i].getPosition())) {
                liveMarkers[i].setMap(null);
                removed.push(liveMarkers);
            }
        }

        //Remove from livemarkers
        for (var i in removed) {
            liveMarkers.splice(liveMarkers.indexOf(removed[i]));
        }

        return liveMarkers;
    }
}

/**
 * Keeps track of which marker is being shown on the map
 */
var activeMarker = undefined;

/**
 * Places a marker on the map at the specified lat / lon
 * location. Only places one though, deletes old.
 * @param map Google maps object
 * @param lat latitude
 * @param lon longitude
 */
function placeMarkerOnce(map, lat, lon) {
    if (activeMarker !== undefined) {
        //Remove from map
        activeMarker.setMap(null);
        activeMarker = undefined;
    }

    var pinIcon = new google.maps.MarkerImage(
        "/images/map-icon.svg",
        null,
        null,
        null,
        new google.maps.Size(50, 50)
    );
    activeMarker = new google.maps.Marker({
        position: { lat: lat, lng: lon },
        map: map,
        icon: pinIcon
    });

    var center = new google.maps.LatLng(lat, lon);
    map.panTo(center);
}

/**
 * Gets the active marker's latitude and longitude
 * in the following array format [lat, lng]
 * @returns {Array} lat / lng array
 */
function getMarkerLatLng() {
    return [activeMarker.getPosition().lat(), activeMarker.getPosition().lng()];
}
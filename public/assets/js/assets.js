/**
 * Created by CChristie on 8/7/2016.
 */

/**
 * Finds featured listings by searching for listings close by the passed latitude / longitude.
 * Expected json response: [{ distance, lat_long, name, city, state }]
 *
 * @param latLon latitude and longitude coordinates to search for
 * @param amount amount of results to be returned
 * @param callback called when the results have been found
 *
 * @return an array of returned listings (empty if none were found)
 */
function getFeaturedListingForLocation(latLon, amount, callback) {
    var split = latLon.split(",");
    var lat = split[0];
    var lon = split[1];

    $.ajax({
        url: 'featuredListings',
        type: 'POST',
        data: { 'lat' : lat, 'lon' : lon, 'amount' : amount },
        dataType: 'json',
        headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') },
        success: function(data) {
            callback(data);
        }, error: function(data) {
            console.log(data);
        }
    })
}

/**
 * Writes an error message to the top of the document.
 * Scrolls page to the top
 * @param message message to display
 * @param type
 * @param selector
 */
function logMessage(message, type, selector) {
    selector = '#error-container';
    var $container = $(selector);

    if (type == "error") {
        if (!$container.hasClass("error")) $container.addClass("error");
        $container.removeClass("success");
    } else {
        if (!$container.hasClass("success")) $container.addClass("success");
        $container.removeClass("error");
    }

    $container.css("display", "block"); //Show
    $container.find("h6").text(message.responseText == 'undefined' ? message : message.responseText);

    window.scrollTo(0, 0); //Scroll to the top
}

//-- Hide error container on "X" click --//
$("#error-container span").click(function(e) {
    e.preventDefault();
    $(this).parent().css("display", "none");
});

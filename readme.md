# Leaftopia Internal Website to find a dispensary, doctor and product nearby your location
###### The Laravel+AngularJS application running on Digital Ocean Ubuntu/Apache Server
***

## Technologies

* Laravel 5.3
* AngularJS 1.5
* Gulp
* Google Map API
* LAMP stack (linux+Apache+MySQL+PHP)